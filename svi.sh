#!/bin/bash
#Copyright (C) 2017, Ville Jouppi <jope@jope.fi>
#All rights reserved.
#
#Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
#
#1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
#
#2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
#
#3. Neither the name of the copyright owner nor the names of contributors to this software may be used to endorse or promote products derived from this software without specific prior written permission.
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#set -x
usage() {
echo "Usage: $0 OPTION...
Dumps DSK and raw images from a 5.25\" floppy connected to a KryoFLux.

Required parameters:
  -n FILENAME    Make a single DSK with this name

Optional parameters:
  -r RETRYCOUNT  Numeric retry value (default is 5)
  -a             Archive raw tracks (default is DSK only)"
exit 1
}

RETRIES=5
ARCHIVE=0
ACCEPTCHARS='[[:alnum:]].\-_'
T=""

if [ $# -gt 0 ];then
  OPTS=$(getopt -o har:n: -- "$@")
  [ $? -gt 0 ] && exit $?
  eval set -- "$OPTS"
  while true ; do
    case "$1" in
      -r) RETRIES="$2" ; shift 2 ;;
      -n) T="$(echo $2|tr " " "_"|tr -cd $ACCEPTCHARS)" ; T=${T%.dsk} ; shift 2 ;;
      -h) usage ;;
      -a) ARCHIVE=1 ; shift ;;
      --) shift ; break ;;
    esac
  done
fi

[ -z "$T" ] && echo "No filename!" && usage

[ $ARCHIVE -eq 1 ] && ( [ -d "dump_raw" ] || mkdir dump_raw )
[ -d "dump_disks" ] || mkdir dump_disks

if [ $ARCHIVE -eq 0 ]; then
  ./dtc -p -f${T}/${T}boot -s0 -e0 -os0 -oe0 -z0 -n+18 -g0 -k2 -i3 -f${T}/${T}data -s2 -e78 -os1 -z1 -n+17 -g0 -k2 -i4 -r5 -l8 -t$RETRIES
  cat ${T}/${T}boot_s0 ${T}/${T}data_s0 > dump_disks/${T}.dsk
  rm ${T}/${T}boot* ${T}/${T}rest*
else
  ./dtc -p -f"${T}/${T}" -e78 -g0 -k2 -i0 -f${T}/${T}boot -s0 -e0 -os0 -oe0 -z0 -n+18 -g0 -k2 -i3 -f${T}/${T}data -s2 -e78 -os1 -z1 -n+17 -g0 -k2 -i4 -r5 -l8 -t$RETRIES
  cat ${T}/${T}boot_s0 ${T}/${T}data_s0 > dump_disks/${T}.dsk
  rm ${T}/${T}boot* ${T}/${T}rest*
  zip -r9 "dump_raw/${T}.zip" "${T}" > /dev/null
fi

rm -rf "${T}"

exit 0
