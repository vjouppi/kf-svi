# README #

### What is this repository for? ###

* This is an SVI 318/328 DSK dumper script for KryoFlux
* Run it with ./svi.sh -h to get help

### How do I get set up? ###

* You need to have dtc set up so that it and its libs are in the path
* Bash
* Coreutils
* Grep
* The Zip archiver
